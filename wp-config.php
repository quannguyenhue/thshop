<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_thshop');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'WwP@)^s-/o9iI l~H|UPUP?#G$i17z[u+M7q5QwVGcD]Nhd 1bs5gmSebAUBY8iE');
define('SECURE_AUTH_KEY',  '-7HaN>t{wr_+tO6#5v(06JE|jA6,{;qtc)i]OYy[s|Zfofwg]Aeb16Du$OxOjTE{');
define('LOGGED_IN_KEY',    'RUd#q+R*~+ =YX2j!V{3VTrR=T)<v|azg`={$UL$hp,Vjeqo+@z}b.j-U^B$<uc|');
define('NONCE_KEY',        'eb.mVv|;rUpF:O,@u o[j.kCgfg|:t7ua/xiT,Uf1ss(2SxV+(gQT5E$dCBb>b~!');
define('AUTH_SALT',        'DY`x|6 j{@-ox`7us8rkHi{)4B6.O(ic>?m:}|L)Qw>JQQ).}wy88z M{LUJk~AW');
define('SECURE_AUTH_SALT', 'q<Wz.gIkMYA35,!}$yfuV[-c.qBs7H.%*||f+Ai.ZoPcZ#@t_+wyev68!{XBH,T|');
define('LOGGED_IN_SALT',   '2-;:GM&lQYDc,<{4Xd(isT$>y%|}L>*m$}_]R;p{yYx&,+#+w9:ik#Uz0A5 }uUR');
define('NONCE_SALT',       '2u|.5^r.XMT<z-a2s<r,iBXL*S$.U<7dh!-[| :;qc|YL1>%pNHqs?=d%o7|SY#d');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
